LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

-- The entity of your testbench. No ports declaration in this case. 
ENTITY Assignment5_tb IS 
END ENTITY; 

ARCHITECTURE testbench OF Assignment5_tb IS 
	-- The component declaration should match your entity. 
	-- It is very important that the name of the component and the ports 
	-- (remember direction of ports!) match your entity! Please notice that 
	-- the code below probably does not work for your design without 
	-- modifications. 
	COMPONENT AssignmentFSM IS 
		PORT ( 
			CLOCK_50	:IN std_logic;
			KEY0		:IN std_logic;
			KEY3		:IN std_logic;
			LEDR		:OUT std_logic;
			HEX0		:OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			HEX2		:OUT std_logic_vector(6 downto 0)
		); 
	END COMPONENT; 
	-- Signal declaration. These signals are used to drive your inputs and 
	-- store results (if required). 
	SIGNAL CLOCK_50_tb		: STD_LOGIC;
	SIGNAL KEY0_tb			: std_logic := '0';
	SIGNAL KEY3_tb			: std_logic := '0';
	SIGNAL LEDR_tb		 	: STD_LOGIC;
	SIGNAL HEX0_tb 			: STD_LOGIC_VECTOR(6 DOWNTO 0);
	SIGNAL HEX2_tb 			: STD_LOGIC_VECTOR(6 DOWNTO 0);
	
	BEGIN 
	-- A port map is in this case nothing more than a construction to 
	-- connect your entity ports with your signals. 
	tb: AssignmentFSM PORT MAP (CLOCK_50 => CLOCK_50_tb, KEY0 => KEY0_tb, KEY3 => KEY3_tb, LEDR => LEDR_tb, HEX0 => HEX0_tb, HEX2 => HEX2_tb);  
PROCESS 
	BEGIN 
	CLOCK_50_tb <= '0';
		for i in 0 to 220000 loop
			wait for 1 ns;
			CLOCK_50_tb <= not CLOCK_50_tb;
			if (i = 10) then
				KEY0_tb <= '1';
				KEY3_tb <= '1';
			elsif (i = 50000) then 
				KEY3_tb <= '0';
			elsif (i = 52000) then
				KEY3_tb <= '1';
			elsif (i = 100000) then
				KEY0_tb <= '0';
			elsif (i = 102000) then
				KEY0_tb <= '1';
			elsif (i = 200000 ) then
				KEY3_tb <= '0';
			elsif (i = 202000) then
				KEY3_tb <= '1';
			else
			end if;	
		end loop;
		
	REPORT "Test completed."; 
	-- Wait forever. 
	WAIT;
	END PROCESS; 

END ARCHITECTURE;
