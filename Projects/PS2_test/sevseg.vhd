library ieee;
use ieee.std_logic_1164.all;

entity sevseg is 
port ( 
	VARSW	: in std_logic_vector(3 downto 0); 
	VARHEX	: out std_logic_vector(6 downto 0)
);
end sevseg;

architecture sevseg of sevseg is
begin
	
		VARHEX <= Not("0000000") when VARSW = "0000" else	
				Not("0111111") when VARSW = "0001" else
				Not("0000110") when VARSW = "0010" else
				Not("1011011") when VARSW = "0011" else
				Not("1001111") when VARSW = "0100" else
				Not("1100110") when VARSW = "0101" else
				Not("1101101") when VARSW = "0110" else
				Not("1111101") when VARSW = "0111" else
				Not("0000111") when VARSW = "1000" else
				Not("1111111") when VARSW = "1001" else
				Not("1101111") when VARSW = "1010" else
				Not("1110111") when VARSW = "1011" else
				Not("1111100") when VARSW = "1100" else
				Not("0111001") when VARSW = "1101" else
				Not("1011110") when VARSW = "1110" else
				Not("1111001") when VARSW = "1111" else -- Alles min 1
				Not("1110001") when VARSW = "1111" else
				Not("0000000");
		
	
end sevseg;
