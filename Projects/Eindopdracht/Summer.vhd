library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity summer is 
port(
	CLK				: in std_logic;
	SumNow			: in std_logic;
	FirstNum		: in std_logic_vector(11 downto 0);
	FirstSigned		: in std_logic;
	SecondNum		: in std_logic_vector(7 downto 0);
	SecondSigned	: in std_logic;
	Reset			: in std_logic;
	SUM			: out std_logic_vector(11 downto 0);
	OutputSigned	: out std_logic
);
end summer;

architecture Behaviour of summer is
	signal Calculate: std_logic := '0';
begin
	process (SumNow, CLK)
	variable CountClk	: integer range 0 to 500;
	variable StartCount	: boolean := false;
	begin
		if rising_edge(SumNow) then
			StartCount := true;
			CountClk := 0;
		end if;
		if StartCount and rising_edge(CLK) then
			CountClk := CountClk + 1;
			if CountClk = 500 then
				StartCount := false;
				Calculate <= '1';
			end if;
		end if;
	end process;

	process (FirstNum, FirstSigned, Calculate, SecondNum, SecondSigned, Reset)
	variable Outcome : integer := 0;
	constant Max: integer := 4095;
	constant MINIMUM: integer := -4095;
	begin
		if rising_edge(Reset) then
			Outcome := 0;
			SUM <= std_logic_vector(to_unsigned(Outcome, SUM'length));
			OutputSigned <= '0';
		end if;
		if rising_edge(Calculate) then --Calculate Outcome
			if FirstSigned = '1' and SecondSigned = '0' then
				Outcome := (to_integer(signed(FirstNum)) + to_integer(unsigned(SecondNum)));
			elsif FirstSigned = '0' and SecondSigned = '1' then
				Outcome := (to_integer(unsigned(FirstNum)) + to_integer(signed(SecondNum)));
			elsif FirstSigned = '1' and SecondSigned = '1' then
				Outcome := (to_integer(signed(FirstNum)) + to_integer(signed(SecondNum)));
			else
				Outcome := (to_integer(unsigned(FirstNum)) + to_integer(unsigned(SecondNum)));
			end if;	
			if Outcome > Max then
				SUM <= std_logic_vector(to_unsigned(Max, SUM'length));
				OutputSigned <= '0';
			elsif Outcome < MINIMUM then
				SUM <= std_logic_vector(to_signed(MINIMUM, SUM'length));
				OutputSigned <= '1';
			elsif Outcome < 0 then
				SUM <= std_logic_vector(to_signed(Outcome, SUM'length));
				OutputSigned <= '1';
			else
				SUM <= std_logic_vector(to_unsigned(Outcome, SUM'length));
				OutputSigned <= '0';
			end if;	
		end if;
	end process;
end Behaviour;