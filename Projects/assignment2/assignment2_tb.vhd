LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all; 

-- The entity of your testbench. No ports declaration in this case. 
ENTITY assignment2_tb IS
END ENTITY;
ARCHITECTURE testbench OF assignment2_tb IS

-- The component declaration should match your entity.
-- It is very important that the name of the component and the ports 
-- (remember direction of ports!) match your entity! Please notice that 
-- the code below probably does not work for your design without 
-- modifications. 
COMPONENT assignment2 IS
PORT (
SW  : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
HEX0  : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
LEDR  : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
);
END COMPONENT;

-- Signal declaration. These signals are used to drive your inputs and
-- store results (if required).
SIGNAL SW_tb  : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL HEX0_tb  : STD_LOGIC_VECTOR(6 DOWNTO 0);
SIGNAL LEDR_tb  : STD_LOGIC_VECTOR(3 DOWNTO 0);
BEGIN
-- A port map is in this case nothing more than a construction to 
-- connect your entity ports with your signals.
tb: assignment2 PORT MAP (SW => SW_tb, HEX0 => HEX0_tb, LEDR => LEDR_tb); 
PROCESS
BEGIN
-- Initialize signals.
SW_tb <= "0000";
-- Loop through values. 
FOR I IN 0 TO 15 LOOP
WAIT FOR 10 ns;
-- Increment by one.
SW_tb <= STD_LOGIC_VECTOR(UNSIGNED(SW_tb) + 1);
END LOOP;
REPORT "Test completed.";
-- Wait forever.
WAIT;
END PROCESS;
END ARCHITECTURE;