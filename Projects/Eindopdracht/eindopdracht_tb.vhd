LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all; 

ENTITY eindopdracht_tb IS
END ENTITY;

ARCHITECTURE testbench OF eindopdracht_tb IS

    COMPONENT Eindopdracht IS
    PORT(
        CLOCK_50:	IN	STD_LOGIC;			            --50MHz clock input
	    KEY0:		IN	STD_LOGIC;			            --reset button
	    PS2_CLK:	INOUT	STD_LOGIC;			        --clock signal from PS2 mouse
	    PS2_DAT:	INOUT	STD_LOGIC;			        --data signal from PS2 mouse
	    LEDR3:		OUT	STD_LOGIC;			            --Sign bit X
	    LEDR9:		OUT	STD_LOGIC;			            --Sign bit Y
	    HEX0:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--LSB X
	    HEX1:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--middleBits X
	    HEX2:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--MSB X
	    HEX3:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--LSB Y
	    HEX4:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--MiddleBits Y
	    HEX5:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0)	--MSB Y
    );
    END COMPONENT;

    SIGNAL CLK_tb:      STD_LOGIC := '0';
    SIGNAL KEY_tb:      STD_LOGIC;
    SIGNAL PS2_CLK_tb:  STD_LOGIC;
    SIGNAL PS2_DAT_tb:  STD_LOGIC;
    SIGNAL LEDR3_tb:    STD_LOGIC;
    SIGNAL LEDR9_tb:   	STD_LOGIC;
    SIGNAL HEX0_tb:    	STD_LOGIC_VECTOR(6 DOWNTO 0);
    SIGNAL HEX1_tb:    	STD_LOGIC_VECTOR(6 DOWNTO 0);
    SIGNAL HEX2_tb:    	STD_LOGIC_VECTOR(6 DOWNTO 0);
    SIGNAL HEX3_tb:    	STD_LOGIC_VECTOR(6 DOWNTO 0);
    SIGNAL HEX4_tb:    	STD_LOGIC_VECTOR(6 DOWNTO 0);
    SIGNAL HEX5_tb:    	STD_LOGIC_VECTOR(6 DOWNTO 0);

    CONSTANT clockPeriod:   time := 20 ns; --50MHZ sys clock
    CONSTANT PS2Period:     time := 80 us; --12.5kHz ps2 freq.

    --FROM http://www.xess.com/static/media/projects/ps2_ctrl.pdf page 6
    function Even (V : std_logic_vector) return std_logic is
        variable p : std_logic := '0';
    begin
        for i in V'range loop p := p xor V(i); end loop; return p;
    end function;

BEGIN

    testbench: Eindopdracht PORT MAP (CLOCK_50 => CLK_tb, 
                                        KEY0 => KEY_tb, 
                                        PS2_CLK => PS2_CLK_tb, 
                                        PS2_DAT => PS2_DAT_tb, 
                                        LEDR3 => LEDR3_tb, 
                                        LEDR9 => LEDR9_tb, 
                                        HEX0 => HEX0_tb, 
                                        HEX1 => HEX1_tb, 
                                        HEX2 => HEX2_tb, 
                                        HEX3 => HEX3_tb, 
                                        HEX4 => HEX4_tb, 
                                        HEX5 => HEX5_tb);


    CLK_tb <= NOT CLK_tb AFTER (clockPeriod/2); --makes the clock oscilate seperate from the rest
    KEY_tb <= '0', '1' AFTER 10 ms; --turns off reset after 20 ms

    PROCESS
        -- from http://www.xess.com/static/media/projects/ps2_ctrl.pdf page 7
        PROCEDURE SendData (D : STD_LOGIC_VECTOR(7 DOWNTO 0); Err: STD_LOGIC := '0') IS
        BEGIN
            PS2_CLK_tb <= 'H';
            PS2_DAT_tb <= 'H';
            WAIT FOR PS2Period;
            --start bit
            PS2_DAT_tb <= '0';
            WAIT FOR (PS2Period / 2);
            PS2_CLK_tb <= '0'; 
            WAIT FOR (PS2Period / 2);
            PS2_CLK_tb <= '1';
            --time for data
            FOR i IN 0 TO 7 LOOP
                PS2_DAT_tb <= D(i);
                WAIT FOR (PS2Period / 2);
                PS2_CLK_tb <= '0';
                WAIT FOR (PS2Period / 2);
                PS2_CLK_tb <= '1';
            END LOOP;
            -- Odd parity bit
            PS2_DAT_tb <= Err XOR NOT Even(D);
            WAIT FOR (PS2Period / 2);
            PS2_CLK_tb <= '0'; 
            WAIT FOR (PS2Period / 2);
            PS2_CLK_tb <= '1';
            -- Stop bit
            PS2_DAT_tb <= '1';
            WAIT FOR (PS2Period / 2);
            PS2_CLK_tb <= '0'; 
            WAIT FOR (PS2Period / 2);
            PS2_CLK_tb <= 'H';
            PS2_DAT_tb <= 'H';
            WAIT FOR (PS2Period * 3);
        END PROCEDURE SendData;
    BEGIN
        PS2_CLK_tb <= 'H';
        PS2_DAT_tb <= 'H';
        WAIT FOR 11 ms;
        --startup before stream:
        SendData(x"FA", '0');
        SendData(x"AA", '0');
        SendData(x"00", '0');
        WAIT FOR PS2Period * 10; --wait because host is sending a report command (0xF4)
        SendData(x"FA", '0');
        --datastream can be started here!!
        --first packet (sends 0 change)
        SendData(x"08", '0'); --bit 3 must always be 1
        SendData(x"00", '0');
        SendData(x"00", '0');

        REPORT "End of sim!";
        WAIT;

    END PROCESS;

END ARCHITECTURE;