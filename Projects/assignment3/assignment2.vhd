LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY assignment2 IS
PORT(
	SW:		IN  std_logic_vector(3 downto 0);
	HEX0:	OUT std_logic_vector(6 downto 0)
);
END assignment2;

ARCHITECTURE implementation OF assignment2 IS
BEGIN
	
	--could be inverted due to being a GND switch instead of Vcc switch
	WITH SW SELECT
	HEX0 <= NOT"0111111" WHEN "0000", -- 0
		NOT"0000110" WHEN "0001", -- 1
		NOT"1011011" WHEN "0010", -- 2
		NOT"1001111" WHEN "0011", -- 3
		NOT"1100110" WHEN "0100", -- 4
		NOT"1101101" WHEN "0101", -- 5
		NOT"1111101" WHEN "0110", -- 6
		NOT"0000111" WHEN "0111", -- 7
		NOT"1111111" WHEN "1000", -- 8
		NOT"1101111" WHEN "1001", -- 9
		NOT"1110111" WHEN "1010", -- A
		NOT"1111100" WHEN "1011", -- b
		NOT"0111001" WHEN "1100", -- C
		NOT"1011110" WHEN "1101", -- d
		NOT"1111001" WHEN "1110", -- E
		NOT"1110001" WHEN OTHERS; -- F
END implementation;