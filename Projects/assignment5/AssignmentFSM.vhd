-- Hogeschool Rotterdam
-- J. Roeloffs
-- Moore FSM.
--

-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
-------------------------------------------------------------------------------
ENTITY AssignmentFSM  IS
PORT (
	CLOCK_50		: IN std_logic;
	KEY3			: IN std_logic; --
	KEY0			: IN std_logic;		
	LEDR			: OUT STD_LOGIC;	 
	HEX0			: OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
	HEX2 			: OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
);
END AssignmentFSM;
-------------------------------------------------------------------------------
ARCHITECTURE behavior OF AssignmentFSM IS
	TYPE state IS (IDLE, ONE, TWO);
	SIGNAL pr_state, nx_state : state;

component flashingLED
Port( 	CLOCK_50	: IN std_logic;
	LEDEnable	: IN STD_LOGIC;
	speedSelect	: IN STD_LOGIC;
	LEDR		: OUT std_logic
);
end component;

component hexDisplay
Port( 	CLOCK_50		: IN std_logic;
	reset			: IN std_Logic;
	HEX0			: OUT STD_logic_vector(6 downto 0);
	countDone		: OUT std_logic
);
end component;

signal A_rising_edge_ff : std_logic_vector(1 downto 0):="00";
signal A_rising_edge : std_logic;
signal LEDenable: STD_LOGIC;
signal speedSelect: STD_LOGIC;
signal reset: STD_LOGIC;
signal countDone: STD_LOGIC:= '0';

BEGIN
G2: flashingLED port map (CLOCK_50, LEDenable, speedSelect, LEDR);
G3: hexDisplay port map (CLOCK_50, reset, HEX0, countDone);
-----------------------------Lower section of FSM:-----------------------------
PROCESS(CLOCK_50, KEY3, nx_state)
BEGIN
	IF (KEY3 = '0') THEN  --0
		pr_state <= IDLE;
	ELSIF (rising_edge(CLOCK_50)) THEN
		pr_state <= nx_state;
	END IF;
END PROCESS;
-----------------------------Upper section of FSM:-----------------------------
PROCESS(pr_state, KEY3, A_rising_edge, countDone )
BEGIN
	CASE pr_state IS
		WHEN IDLE =>
			reset <= '1';
			LEDEnable <= '1';
			speedSelect <= '0';
			HEX2 <= "1000000";
			IF (A_rising_edge = '1') THEN
				nx_state <= ONE;
		  	ELSE
				nx_state <= IDLE;
		  	END IF;
		WHEN ONE =>
			LEDEnable <= '0';
			speedSelect <= '0';
			reset <= '0';
			HEX2 <= "1111001";
		  	IF (countDone = '1') THEN
				nx_state <= TWO;
			ELSE
				nx_state <= ONE;
			END IF;
		WHEN TWO =>
			reset <= '1';
			LEDEnable <= '1';
			speedSelect <= '1';
			HEX2 <= "0100100";

			IF (KEY3 = '0') THEN		-- 0
				nx_state <= IDLE;
			ELSE
				nx_state <= TWO;
			END IF;
		WHEN OTHERS =>
		  nx_state <= IDLE;
	END CASE;
END PROCESS;

PROCESS(CLOCK_50, KEY0, A_rising_edge_ff)
BEGIN
	if (rising_edge(CLOCK_50)) then
		A_rising_edge_ff(0) <= KEY0;
                A_rising_edge_ff(1) <= A_rising_edge_ff(0);
                if (A_rising_edge_ff = "01")then
                    A_rising_edge <= '1';
                else
                    A_rising_edge <= '0';
                end if; 
	end if;
END PROCESS;
-------------------------------------------------------------------------------
END behavior;
-------------------------------------------------------------------------------
