LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY PS2_test IS
PORT(
	CLOCK_50:	IN	STD_LOGIC;			--50MHz clock input
	KEY0:		IN	STD_LOGIC;			--reset button
	PS2_CLK:	INOUT	STD_LOGIC;			--clock signal from PS2 mouse
	PS2_DAT:	INOUT	STD_LOGIC;			--data signal from PS2 mouse
	LEDR:		OUT	STD_LOGIC;			--New data
	HEX0:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--LSB X movement
	HEX1:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--MSB X movement
	HEX4:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--LSB Y movement
	HEX5:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0)	--MSB Y movement
);
END PS2_test;

ARCHITECTURE mousetest OF PS2_test IS

	COMPONENT ps2_mouse IS
	PORT(
		clk:		IN	STD_LOGIC;	--system clock input
		reset_n:	IN	STD_LOGIC;	--active low asynchronous reset
		ps2_clk:	INOUT	STD_LOGIC;	--clock signal from PS2 mouse
		ps2_data:	INOUT	STD_LOGIC;	--data signal from PS2 mouse
		mouse_data:	OUT	STD_LOGIC_VECTOR(23 DOWNTO 0);	--data received from mouse
		mouse_data_new:	OUT	STD_LOGIC	--new data packet available flag
	);
	END COMPONENT;

	COMPONENT sevseg IS
	PORT(
		VARSW	: in std_logic_vector(3 downto 0); 
		VARHEX	: out std_logic_vector(6 downto 0)
	);
	END COMPONENT;

	SIGNAL DATA_OUTPUT:	STD_LOGIC_VECTOR(23 DOWNTO 0);
	SIGNAL NEWDATA:		STD_LOGIC;
	SIGNAL CLKIN:		STD_LOGIC;
	SIGNAL RESET:		STD_LOGIC;

	SIGNAL DRIVER0:		STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL DRIVER1:		STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL DRIVER4:		STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL DRIVER5:		STD_LOGIC_VECTOR(3 DOWNTO 0);

BEGIN
	--port maps
	test: ps2_mouse PORT MAP (clk => CLKIN, reset_n => RESET, ps2_clk => PS2_CLK, ps2_data => PS2_DAT, mouse_data(23 DOWNTO 0) => DATA_OUTPUT(23 DOWNTO 0), mouse_data_new => NEWDATA);
	DRIV0: sevseg PORT MAP (DATA_OUTPUT(11 DOWNTO 8), HEX0(6 DOWNTO 0));
	DRIV1: sevseg PORT MAP (DATA_OUTPUT(15 DOWNTO 12), HEX1(6 DOWNTO 0));
	DRIV4: sevseg PORT MAP (DATA_OUTPUT(19 DOWNTO 16), HEX4(6 DOWNTO 0));
	DRIV5: sevseg PORT MAP (DATA_OUTPUT(23 DOWNTO 20), HEX5(6 DOWNTO 0));

	--signal connections
	LEDR <= NEWDATA;
	RESET <= KEY0;
	CLKIN <= CLOCK_50;
END ARCHITECTURE;