LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY Eindopdracht IS
PORT(
	CLOCK_50:	IN	STD_LOGIC;			--50MHz clock input
	KEY0:		IN	STD_LOGIC;			--reset button
	PS2_CLK:	INOUT	STD_LOGIC;			--clock signal from PS2 mouse
	PS2_DAT:	INOUT	STD_LOGIC;			--data signal from PS2 mouse
	LEDR3:		OUT	STD_LOGIC;			--Sign bit X
	LEDR9:		OUT	STD_LOGIC;			--Sign bit Y
	HEX0:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--LSB X
	HEX1:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--middleBits X
	HEX2:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--MSB X
	HEX3:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--LSB Y
	HEX4:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0);	--MiddleBits Y
	HEX5:		OUT	STD_LOGIC_VECTOR(6 DOWNTO 0)	--MSB Y
);
END Eindopdracht;

ARCHITECTURE mousetest OF Eindopdracht IS

	COMPONENT ps2_mouse IS
	PORT(
		clk:		IN	STD_LOGIC;	--system clock input
		reset_n:	IN	STD_LOGIC;	--active low asynchronous reset
		ps2_clk:	INOUT	STD_LOGIC;	--clock signal from PS2 mouse
		ps2_data:	INOUT	STD_LOGIC;	--data signal from PS2 mouse
		mouse_data:	OUT	STD_LOGIC_VECTOR(23 DOWNTO 0);	--data received from mouse
		mouse_data_new:	OUT	STD_LOGIC	--new data packet available flag
	);
	END COMPONENT;

	COMPONENT sevseg IS
	PORT(
		VARSW	: in std_logic_vector(3 downto 0); --Input
		VARHEX	: out std_logic_vector(6 downto 0) --Output
	);
	END COMPONENT;

	COMPONENT summer IS
	PORT(
		CLK				: in std_logic; --system clock
		SumNow			: in std_logic; --signal to start summing, most likely "mouse_new_data" from ps2_mous
		FirstNum		: in std_logic_vector(11 downto 0); -- First number, just link to the output of this component.
		FirstSigned		: in std_logic;	-- shows if number is signed
		SecondNum		: in std_logic_vector(7 downto 0); --Second number, link to ps2_mouse
		SecondSigned	: in std_logic;	-- signed or not
		Reset			: in std_logic; -- Reset back to 0
		SUM			: out std_logic_vector(11 downto 0); --outcome of calc
		OutputSigned	: out std_logic --shows if output is signed
	);
	END COMPONENT;

	-- Create Signals
	-- PS signals
	SIGNAL PS_OUTPUT:	STD_LOGIC_VECTOR(23 downto 0);
	SIGNAL NEWDATA:		STD_LOGIC;
	SIGNAL PS_CLOCK:	STD_LOGIC;
	SIGNAL PS_RESET:	STD_LOGIC;
	--SUMX Signals
	SIGNAL SUMX_CLK:	STD_LOGIC;
	SIGNAL SUMY_CLK:	STD_LOGIC;
	SIGNAL X_RESET:		STD_LOGIC;
	SIGNAL Y_RESET:		STD_LOGIC;
	SIGNAL X_OUTPUT:	STD_LOGIC_VECTOR(11 DOWNTO 0);
	SIGNAL Y_OUTPUT:	STD_LOGIC_VECTOR(11 DOWNTO 0);
	SIGNAL X_OUT_SIGN:	STD_LOGIC;
	SIGNAL Y_OUT_SIGN:	STD_LOGIC;

BEGIN
	--port maps
	-- ps2
	mouse: ps2_mouse GENERIC MAP (clk_freq => 50000000, ps2_debounce_counter_size => 8) PORT MAP (clk => PS_CLOCK, reset_n => PS_RESET, ps2_clk => PS2_CLK, ps2_data => PS2_DAT, mouse_data(23 DOWNTO 0) => PS_OUTPUT(23 DOWNTO 0), mouse_data_new => NEWDATA);
	-- HEX
	DRIVE0: sevseg PORT MAP (X_OUTPUT(3 DOWNTO 0), HEX0(6 DOWNTO 0)); --X 0-3
	DRIVE1: sevseg PORT MAP (X_OUTPUT(7 DOWNTO 4), HEX1(6 DOWNTO 0)); --X 4-7
	DRIVE2: sevseg PORT MAP (X_OUTPUT(11 DOWNTO 8), HEX2(6 DOWNTO 0)); --X 8-11	
	DRIVE3: sevseg PORT MAP (Y_OUTPUT(3 DOWNTO 0), HEX3(6 DOWNTO 0)); --Y 0-3
	DRIVE4: sevseg PORT MAP (Y_OUTPUT(7 DOWNTO 4), HEX4(6 DOWNTO 0)); --Y 4-7
	DRIVE5: sevseg PORT MAP (Y_OUTPUT(11 DOWNTO 8), HEX5(6 DOWNTO 0)); --Y 8-11
	--SUM
	SUMX: summer PORT MAP (CLK => SUMX_CLK, SumNow => NEWDATA, FirstNum(11 DOWNTO 0) => X_OUTPUT(11 DOWNTO 0), FirstSigned => X_OUT_SIGN, SecondNum(7 DOWNTO 0) => PS_OUTPUT(15 DOWNTO 8), SecondSigned => PS_OUTPUT(4), Reset => X_RESET, SUM(11 DOWNTO 0) => X_OUTPUT(11 DOWNTO 0), OutputSigned => X_OUT_SIGN);
	SUMY: summer PORT MAP (CLK => SUMY_CLK, SumNow => NEWDATA, FirstNum(11 DOWNTO 0) => Y_OUTPUT(11 DOWNTO 0), FirstSigned => Y_OUT_SIGN, SecondNum(7 DOWNTO 0) => PS_OUTPUT(23 DOWNTO 16), SecondSigned => PS_OUTPUT(5), Reset => Y_RESET, SUM(11 DOWNTO 0) => Y_OUTPUT(11 DOWNTO 0), OutputSigned => Y_OUT_SIGN);
	--LEDs
	LEDR3 <= X_OUT_SIGN;
	LEDR9 <= Y_OUT_SIGN;
	--signal connections
	PS_RESET <= KEY0;
	X_RESET <= KEY0;
	Y_RESET <= KEY0;
	PS_CLOCK <= CLOCK_50;
	SUMX_CLK <= CLOCK_50;
	SUMY_CLK <= CLOCK_50;
END ARCHITECTURE;