Library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity flashingLED is
port (
	CLOCK_50		: IN std_logic;
	LEDEnable		: IN STD_LOGIC;
	speedSelect		: IN STD_LOGIC;
	LEDR			: OUT std_logic
); 
end flashingLED;

architecture Implementation of flashingLED is

signal countLed: natural range 0 to 50000000;
signal countLed1: natural range 0 to 25000000;
signal toggleLed: std_logic :=  '0';


begin
	process(CLOCK_50, LEDEnable, speedSelect)	
	begin
		if rising_edge(CLOCK_50) then
			if (countLed < 500) then  --50000000
				countLed <= countLed + 1;
			else
				countLed <= 0;
			end if;
		end if;
		
		if rising_edge(CLOCK_50) then
			if (countLed1 < 250) then  --25000000
				countLed1 <= countLed1 + 1;
			else
				countLed1 <= 0;
			end if;
		end if;
		
		
		if LEDEnable = '1' then
		if(speedSelect = '0') then
			if(countLed < 125) then --12500000) 
				toggleLed <= '1';
			else
				toggleLed <= '0';
			end if;
		else 
			if((countLed1) < 187) then --18750000
				toggleLed <= '1';
			else
				toggleLed <= '0';
			end if;
		end if;
		else
			toggleLed <= '0';
		end if;

	end process;

	LEDR <= toggleLed;
end Implementation;
	
