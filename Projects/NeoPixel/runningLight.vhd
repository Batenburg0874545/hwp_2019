LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.Numeric_Std.all;

ENTITY runningLight IS
PORT(
	CLK:			IN	STD_LOGIC;
	CLKSPEED:		IN 	STD_LOGIC;
	LIGHTDIRECTION:	IN	STD_LOGIC;
	DATA_NEXT:		IN	STD_LOGIC;
	DATA_RED:		OUT	STD_LOGIC_VECTOR(7 DOWNTO 0);
	DATA_GREEN:		OUT	STD_LOGIC_VECTOR(7 DOWNTO 0);
	DATA_BLUE:		OUT	STD_LOGIC_VECTOR(7 DOWNTO 0);
	DATA_VALID:		OUT	STD_LOGIC
);
END runningLight;

ARCHITECTURE behaviour OF runningLight IS
    SIGNAL MAINPIXLOC:      STD_LOGIC_VECTOR(3 DOWNTO 0) := "0000";
    SIGNAL VALIDSTAT:       STD_LOGIC := '0';
    SIGNAL FRAMENOW:        STD_LOGIC := '0';
    SIGNAL PART_RED:        STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";
    SIGNAL PART_GREEN:      STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";
    SIGNAL PART_BLUE:       STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";
BEGIN
    DATA_VALID <= VALIDSTAT;
    DATA_RED <= PART_RED;
    DATA_GREEN <= PART_GREEN;
    DATA_BLUE <= PART_BLUE;

    --process for changing location of the main pixel
    PROCESS(CLKSPEED, LIGHTDIRECTION)
        variable locationCounter: integer range 0 to 11;
    BEGIN
        IF RISING_EDGE(CLKSPEED) AND LIGHTDIRECTION = '1' THEN
            IF locationCounter = 11 THEN
                locationCounter := 0;
                MAINPIXLOC <= std_logic_vector(to_unsigned(locationCounter, MAINPIXLOC'length));
            ELSE
                locationCounter := locationCounter + 1;
                MAINPIXLOC <= std_logic_vector(to_unsigned(locationCounter, MAINPIXLOC'length));
            END IF;
        ELSIF RISING_EDGE(CLKSPEED) AND LIGHTDIRECTION = '0' THEN
            IF locationCounter = 0 THEN
                locationCounter := 11;
                MAINPIXLOC <= std_logic_vector(to_unsigned(locationCounter, MAINPIXLOC'length));
            ELSE
                locationCounter := locationCounter - 1;
                MAINPIXLOC <= std_logic_vector(to_unsigned(locationCounter, MAINPIXLOC'length));
            END IF;
        END IF;
    END PROCESS;

    --process to create a 50Hz clock for the "framerate"
    PROCESS(CLK)
        variable freqCounter: integer range 0 to 500000;
    BEGIN
        IF RISING_EDGE(CLK) THEN
            freqCounter := freqCounter + 1;
        END IF;
        IF freqCounter = 500000 THEN
            freqCounter := 0;
            FRAMENOW <= NOT FRAMENOW;
        END IF;
    END PROCESS;

    --process for rendering
    PROCESS(CLKSPEED, DATA_NEXT, VALIDSTAT, FRAMENOW)
        variable currentLoc: integer range 0 to 12 := 0;
        constant mainColorRed: integer := 26;
        constant mainColorGreen: integer := 125;
        constant mainColorBlue:  integer := 94;
        constant colorBlank: integer := 0;
    BEGIN
        --start of a frame
        IF FRAMENOW = '1' AND currentLoc = 0 AND VALIDSTAT = '0' THEN
            IF (currentLoc = to_integer(unsigned(MAINPIXLOC))) THEN
                PART_RED <= std_logic_vector(to_unsigned(mainColorRed, DATA_RED'length));
                PART_GREEN <= std_logic_vector(to_unsigned(mainColorGreen, DATA_GREEN'length));
                PART_BLUE <= std_logic_vector(to_unsigned(mainColorRed, DATA_BLUE'length));
            ELSE
                PART_RED <= std_logic_vector(to_unsigned(colorBlank, DATA_RED'length));
                PART_GREEN <= std_logic_vector(to_unsigned(colorBlank, DATA_GREEN'length));
                PART_BLUE <= std_logic_vector(to_unsigned(colorBlank, DATA_BLUE'length));
            END IF;

            VALIDSTAT <= '1';
            currentLoc := currentLoc + 1;

        --continue till all pixels are set
        ELSIF RISING_EDGE(DATA_NEXT) AND VALIDSTAT = '1' THEN
            IF currentLoc <= 11 THEN
                IF (currentLoc = to_integer(unsigned(MAINPIXLOC))) THEN
                    PART_RED <= std_logic_vector(to_unsigned(mainColorRed, DATA_RED'length));
                    PART_GREEN <= std_logic_vector(to_unsigned(mainColorGreen, DATA_GREEN'length));
                    PART_BLUE <= std_logic_vector(to_unsigned(mainColorRed, DATA_BLUE'length));
                ELSE
                    PART_RED <= std_logic_vector(to_unsigned(colorBlank, DATA_RED'length));
                    PART_GREEN <= std_logic_vector(to_unsigned(colorBlank, DATA_GREEN'length));
                    PART_BLUE <= std_logic_vector(to_unsigned(colorBlank, DATA_BLUE'length));
                END IF;

                currentLoc := currentLoc + 1;

            ELSE
                currentLoc := 0;
                VALIDSTAT <= '0';
            END IF;
        END IF;
    END PROCESS;
END ARCHITECTURE;