LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY pixel_tb IS
END ENTITY;

ARCHITECTURE testbench OF pixel_tb IS

    COMPONENT NeoPixel is
    PORT(
       CLOCK_50:	IN	STD_LOGIC; --50MHZ clock input
        BUTTON:		IN	STD_LOGIC_VECTOR(2 DOWNTO 0); --slower,faster, change direction
        RESET:		IN	STD_LOGIC;
        PIX_OUT:	OUT	STD_LOGIC  --NeoPixel output
    );
    END COMPONENT;

    SIGNAL CLK_tb:  STD_LOGIC;
    SIGNAL BUT_tb:  STD_LOGIC_VECTOR(2 DOWNTO 0);
    SIGNAL RST_tb:  STD_LOGIC;
    SIGNAL PIX_tb:  STD_LOGIC;

BEGIN

    tb: NeoPixel PORT MAP (CLOCK_50 => CLK_tb, BUTTON => BUT_tb, RESET => RST_tb, PIX_OUT => PIX_tb);

    PROCESS
    BEGIN
	    CLK_tb <= '0';
	    BUT_tb <= "000";
	    RST_tb <= '0';

        FOR I IN 0 TO 100000000 LOOP
        WAIT FOR 10 ns;
            CLK_tb <= NOT CLK_tb;
        END LOOP;

	    REPORT "Test complete.";
    	WAIT;
    END PROCESS;

END ARCHITECTURE;