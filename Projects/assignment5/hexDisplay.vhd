library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity hexDisplay is 
port ( 
	CLOCK_50		: IN std_logic;
	reset			: IN std_Logic;
	HEX0			: OUT STD_logic_vector(6 downto 0);
	countDone		: OUT std_logic
);
end hexDisplay;

architecture hexDisplay of hexDisplay is

component sevseg
port(
	VARSW	: IN STD_logic_vector(3 downto 0);
	VARHEX	: OUT STD_logic_vector(6 downto 0)
);

end component;

signal temp: Integer Range 0 to 11;
signal countHex: natural range 0 to 50000000;
signal hex : std_logic_vector(3 downto 0);
signal temp2: std_logic:= '0';

BEGIN
	G1: sevseg port map(hex, HEX0);

	PROCESS(CLOCK_50, reset)
	Begin
		if (reset ='0') then
			if (rising_edge(CLOCK_50)) Then
				if (countHex = 500) then  --50000000
					countHex <= 0;
					temp <= temp + 1;
					temp2 <= '0';

					if (temp = 10) then 
						temp <= 0;
						temp2 <= '1';
					end if;
				else
					countHex <= countHex + 1;
					temp2 <= '0';
				end if;
			end if;
	
		end if;

		if (reset ='1') then
			temp2 <= '0';
			countHex <= 0;
			temp <= 0;
		end if;
	end process;

	countDone <= temp2;
	hex <= STD_logic_vector(to_unsigned(temp , 4));

end hexDisplay;

