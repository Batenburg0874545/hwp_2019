LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY NeoPixel IS
PORT(
	CLOCK_50:	IN	STD_LOGIC; --50MHZ clock input
	BUTTON:		IN	STD_LOGIC_VECTOR(2 DOWNTO 0); --slower,faster, change direction
	RESET:		IN	STD_LOGIC;
	PIX_OUT:	OUT	STD_LOGIC  --NeoPixel output
);
END NeoPixel;

ARCHITECTURE behaviour OF NeoPixel IS

	--WS2812b driver, source: https://github.com/m42uko/ws2812b-vhdl
	COMPONENT ws2812b_phy IS
	PORT(
		-- Global Signals
		clk           : in  std_logic;  -- System clock @ f_clk
		rst           : in  std_logic;  -- Asynchronous reset
		-- Hardware Connection
		so            : out std_logic;  -- Serial output to WS2812B
		-- Data Link
		pixData_red   : in  std_logic_vector(7 downto 0);
		pixData_green : in  std_logic_vector(7 downto 0);
		pixData_blue  : in  std_logic_vector(7 downto 0);
		pixData_valid : in  std_logic;
		pixData_next  : out std_logic
	);
	END COMPONENT;

	--component to select direction and speed
	COMPONENT clockSpeed IS
	PORT(
		CLOCKIN:		IN	STD_LOGIC;
		SPEEDUP:		IN 	STD_LOGIC;
		SPEEDDOWN:		IN	STD_LOGIC;
		DIRECTIONIN: 	IN STD_LOGIC;
		CLOCKOUT:		OUT	STD_LOGIC;
		DIRECTIONOUT:	OUT STD_LOGIC
	);
	END COMPONENT;

	--component to run the running light
	COMPONENT runningLight IS
	PORT(
		CLK:			IN	STD_LOGIC;
		CLKSPEED:		IN 	STD_LOGIC;
		LIGHTDIRECTION:	IN	STD_LOGIC;
		DATA_NEXT:		IN	STD_LOGIC;
		DATA_RED:		OUT	STD_LOGIC_VECTOR(7 DOWNTO 0);
		DATA_GREEN:		OUT	STD_LOGIC_VECTOR(7 DOWNTO 0);
		DATA_BLUE:		OUT	STD_LOGIC_VECTOR(7 DOWNTO 0);
		DATA_VALID:		OUT	STD_LOGIC
	);
	END COMPONENT;

	--movement indicator signals
	SIGNAL LEAPSPEED:	STD_LOGIC; --indicator how fast the light moves past pixels
	SIGNAL PIXELDIR:	STD_LOGIC; --direction the pixel moves
	--comms signals
	SIGNAL NEXTPIX:		STD_LOGIC;
	SIGNAL RED:			STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL GREEN:		STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL BLUE:		STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL VALID:		STD_LOGIC;

BEGIN
	--port maps
	control: clockSpeed PORT MAP (CLOCKIN => CLOCK_50, SPEEDUP => BUTTON(1), SPEEDDOWN => BUTTON(2), DIRECTIONIN => BUTTON(0), CLOCKOUT => LEAPSPEED, DIRECTIONOUT => PIXELDIR);
	runlight: runningLight PORT MAP (CLK => CLOCK_50, CLKSPEED => LEAPSPEED, LIGHTDIRECTION => PIXELDIR, DATA_NEXT => NEXTPIX, DATA_RED(7 DOWNTO 0) => RED(7 DOWNTO 0), DATA_GREEN(7 DOWNTO 0) => GREEN(7 DOWNTO 0), DATA_BLUE(7 DOWNTO 0) => BLUE(7 DOWNTO 0), DATA_VALID => VALID);
	driver: ws2812b_phy PORT MAP (clk => CLOCK_50, rst => RESET, so => PIX_OUT, pixData_red(7 DOWNTO 0) => RED(7 DOWNTO 0), pixData_green(7 DOWNTO 0) => GREEN(7 DOWNTO 0), pixData_blue(7 DOWNTO 0) => BLUE(7 DOWNTO 0), pixData_valid => VALID, pixData_next => NEXTPIX);
END ARCHITECTURE;