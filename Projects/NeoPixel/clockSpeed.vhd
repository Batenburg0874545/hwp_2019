LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.Numeric_Std.all;

ENTITY clockSpeed IS
PORT(
	CLOCKIN:		IN	STD_LOGIC;
	SPEEDUP:		IN 	STD_LOGIC;
	SPEEDDOWN:		IN	STD_LOGIC;
	DIRECTIONIN: 	IN STD_LOGIC;
	CLOCKOUT:		OUT	STD_LOGIC;
	DIRECTIONOUT:	OUT STD_LOGIC
);
END clockSpeed;

ARCHITECTURE behaviour OF clockSpeed IS
	SIGNAL SPEED:			STD_LOGIC_VECTOR(3 DOWNTO 0) := "1000";
	SIGNAL CURRENTDIRECTION:	STD_LOGIC := '0';
	SIGNAL CURRENTCLOCKOUT:		STD_LOGIC := '0';
BEGIN
	DIRECTIONOUT <= CURRENTDIRECTION;
	CLOCKOUT <= CURRENTCLOCKOUT;

	--toggles the direction on the press of the direction button
	PROCESS(DIRECTIONIN)
	BEGIN
		IF RISING_EDGE(DIRECTIONIN) THEN
			CURRENTDIRECTION <= NOT CURRENTDIRECTION;
		END IF;
	END PROCESS;

	--handle speed up and down inputs
	PROCESS (SPEEDUP, SPEEDDOWN)
		variable speedCounter: integer range 1 to 15 := 8;
	BEGIN
		IF SPEEDUP = '0' THEN
			IF speedCounter = 15 THEN
				SPEED <= std_logic_vector(to_unsigned(speedCounter, SPEED'length));
			ELSE
				speedCounter := speedCounter + 1;
				SPEED <= std_logic_vector(to_unsigned(speedCounter, SPEED'length));
			END IF;
		END IF;
		IF SPEEDDOWN = '0' THEN
			IF speedCounter = 1 THEN
				SPEED <= std_logic_vector(to_unsigned(speedCounter, SPEED'length));
			ELSE
				speedCounter := speedCounter - 1;
				SPEED <= std_logic_vector(to_unsigned(speedCounter, SPEED'length));
			END IF;
		END IF;
	END PROCESS;

	--set clock to wanted speed
	PROCESS(CLOCKIN, SPEED)
		variable clockCounter: integer range 0 to 50000000;
	BEGIN
		IF RISING_EDGE(CLOCKIN) THEN
			clockCounter := clockCounter + 1;
		END IF;
		IF (clockCounter >= (50000000/to_integer(unsigned(SPEED)))) THEN
			clockCounter := 0;
			CURRENTCLOCKOUT <= NOT CURRENTCLOCKOUT;
		END IF;
	END PROCESS;
END ARCHITECTURE;
