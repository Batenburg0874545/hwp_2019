library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Assignment3 is 
port ( 
	SW 			: IN STD_logic_vector(7 downto 0);
	HEX0, HEX1, HEX3, HEX5	: OUT STD_logic_vector(6 downto 0) := "1111111";
	KEY			: IN STD_logic_vector(0 downto 0)
);
end Assignment3;

architecture Assignment3 of Assignment3 is
component sevseg
port(
	VARSW	: IN STD_logic_vector(3 downto 0);
	VARHEX	: OUT STD_logic_vector(6 downto 0)
);
end component;

signal hexsum 	: std_logic_vector(4 downto 0);

begin

	G1 :	sevseg port map (SW(7 downto 4),HEX5);
	G2 :	sevseg port map (SW(3 downto 0),HEX3);
	G3 :    sevseg port map (hexsum(3 downto 0), HEX0);
	
	HEX1 <= "1111001" when hexsum(4)='1' else
				"1111111";

	process(KEY)
	begin
		if (KEY = "0") then
			hexsum <= std_logic_vector(resize(unsigned(SW(7 downto 4)), hexsum'length) + unsigned(SW(3 downto 0)));
		end if;
	end process;


end Assignment3;