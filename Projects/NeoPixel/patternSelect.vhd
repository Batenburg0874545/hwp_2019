LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY patternSelect IS
PORT(
	PATTERN0_IN:	IN	STD_LOGIC_VECTOR(23 DOWNTO 0); --patterns to be selected
	PATTERN1_IN:	IN	STD_LOGIC_VECTOR(23 DOWNTO 0);
	PATTERN2_IN:	IN	STD_LOGIC_VECTOR(23 DOWNTO 0);
	PAT_SEL:		IN	STD_LOGIC_VECTOR(2 DOWNTO 0); --select pattern
	VALID_IN:		IN	STD_LOGIC_VECTOR(2 DOWNTO 0); --start sending indicator
	NEXT_IN:		IN	STD_LOGIC;			 --next pixel indicator from driver, goes to only one pattern
	PATTERN_OUT:	OUT	STD_LOGIC_VECTOR(23 DOWNTO 0);
	VALID_OUT:		OUT	STD_LOGIC;
	NEXT_OUT:		OUT	STD_LOGIC_VECTOR(2 DOWNTO 0); --next pixel indicator from driver.
	RESET_OUT:		OUT STD_LOGIC_VECTOR(2 DOWNTO 0) --used to "turn off" the patterns that are not used.
);
END patternSelect;

ARCHITECTURE pattern OF patternSelect IS

BEGIN
	PROCESS(PAT_SEL(2 DOWNTO 0))
	BEGIN
		IF RISING_EDGE(PAT_SEL(0)) THEN
			PATTERN_OUT <= PATTERN0_IN;
			VALID_OUT <= VALID_IN(0);
			NEXT_OUT(0) <= NEXT_IN;
			RESET_OUT <= "110";
		END IF;
		IF RISING_EDGE(PAT_SEL(1)) THEN
			PATTERN_OUT <= PATTERN1_IN;
			VALID_OUT <= VALID_IN(1);
			NEXT_OUT(1) <= NEXT_IN;
			RESET_OUT <= "101";
		END IF;
		IF RISING_EDGE(PAT_SEL(2)) THEN
			PATTERN_OUT <= PATTERN2_IN;
			VALID_OUT <= VALID_IN(2);
			NEXT_OUT(2) <= NEXT_IN;
			RESET_OUT <= "011";
		END IF;
	END PROCESS;

END ARCHITECTURE;