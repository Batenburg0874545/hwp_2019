LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

-- The entity of your testbench. No ports declaration in this case. 
ENTITY Assignment4_tb IS 
END ENTITY; 

ARCHITECTURE testbench OF Assignment4_tb IS 
	-- The component declaration should match your entity. 
	-- It is very important that the name of the component and the ports 
	-- (remember direction of ports!) match your entity! Please notice that 
	-- the code below probably does not work for your design without 
	-- modifications. 
	COMPONENT Assignment4 IS 
		PORT ( 
			CLOCK_50	:IN std_logic;
			HEX0		:OUT std_logic_vector(6 downto 0);
			LEDR		:OUT std_logic
		); 
	END COMPONENT; 
	-- Signal declaration. These signals are used to drive your inputs and 
	-- store results (if required). 
	SIGNAL CLOCK_50_tb		: STD_LOGIC;
	SIGNAL LEDR_tb		 	: STD_LOGIC;
	SIGNAL HEX0_tb 			: STD_LOGIC_VECTOR(6 DOWNTO 0);
	
	BEGIN 
	-- A port map is in this case nothing more than a construction to 
	-- connect your entity ports with your signals. 
	tb: Assignment4 PORT MAP (CLOCK_50 => CLOCK_50_tb, HEX0 => HEX0_tb, LEDR => LEDR_tb);  

	--CLOCK_50_tb <= '1' after 0.5 ns when CLOCK_50_tb = '0' else
			--'0' after 0.5 ns when CLOCK_50_tb = '1';

	--CLOCK_50_tb <= '0';

	PROCESS 
	BEGIN 
	CLOCK_50_tb <= '0';
		for i in 0 to 1000 loop
			wait for 1 ns;
			CLOCK_50_tb <= not CLOCK_50_tb;
		end loop;

	REPORT "Test completed."; 
	-- Wait forever. 
	WAIT;
	END PROCESS; 

END ARCHITECTURE;
