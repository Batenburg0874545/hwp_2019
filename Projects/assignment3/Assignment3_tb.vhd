LIBRARY ieee; 
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all; 

-- The entity of your testbench. No ports declaration in this case. 
ENTITY assignment3_tb IS 
END ENTITY; 

ARCHITECTURE testbench OF assignment3_tb IS 
	-- The component declaration should match your entity. 
	-- It is very important that the name of the component and the ports 
	-- (remember direction of ports!) match your entity! Please notice that 
	-- the code below probably does not work for your design without 
	-- modifications. 
	COMPONENT assignment3 IS 
	PORT ( 
	    SW:  in std_logic_vector(7 downto 0);   
	    HEX5:  out std_logic_vector(6 downto 0);
    	    HEX3:  out std_logic_vector(6 downto 0);
            HEX1:  out std_logic_vector(6 downto 0);
            HEX0:  out std_logic_vector(6 downto 0);
            KEY:  in std_logic_vector(0 downto 0)
); 
	END COMPONENT; 
	-- Signal declaration. These signals are used to drive your inputs and 
	-- store results (if required). 
	SIGNAL SW1_tb : STD_LOGIC_VECTOR(3 DOWNTO 0);
 	SIGNAL SW2_tb : STD_LOGIC_VECTOR(3 DOWNTO 0);
	SIGNAL HEX5_tb : STD_LOGIC_VECTOR(6 DOWNTO 0);
	SIGNAL HEX3_tb : STD_LOGIC_VECTOR(6 DOWNTO 0);
	SIGNAL HEX1_tb : STD_LOGIC_VECTOR(6 DOWNTO 0);
	SIGNAL HEX0_tb : STD_LOGIC_VECTOR(6 DOWNTO 0);
	SIGNAL KEY_tb : STD_LOGIC_vector(0 downto 0); 
	BEGIN 
	-- A port map is in this case nothing more than a construction to 
	-- connect your entity ports with your signals. 
	tb: assignment3 PORT MAP (SW(7 DOWNTO 4) => SW1_tb, SW(3 DOWNTO 0) => SW2_tb, HEX5 => HEX5_tb, HEX3 => HEX3_tb, HEX1 => HEX1_tb, HEX0 => HEX0_tb, KEY => KEY_tb); 
	
	PROCESS 
	BEGIN 
	-- Initialize signals. 
	SW1_tb <= "0000";
	SW2_tb <= "0000";
	KEY_tb <= "1"; 
	-- Loop through values. 
	FOR I IN 0 TO 15 LOOP 
	WAIT FOR 10 ps; 
	-- Increment by one. 
	SW1_tb <= STD_LOGIC_VECTOR(UNSIGNED(SW1_tb) + 1);
	FOR I IN 0 TO 15 LOOP
	WAIT FOR 8 ps;
	SW2_tb <= STD_LOGIC_VECTOR(UNSIGNED(SW2_tb) + 1);
	WAIT FOR 1 ps;
	KEY_tb <= "0";
	WAIT FOR 1 ps;
	KEY_tb <= "1";
	END LOOP;
	END LOOP; 
	REPORT "Test completed."; 
	-- Wait forever. 
	WAIT;
 
END PROCESS; 

END ARCHITECTURE;